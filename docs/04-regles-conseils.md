---
hide:
  - footer
---

# RÈGLES ET CONSEILS

!!! note ""
    Documents à distribuer aux équipes:

    * Règles et conseils <a href="https://forge.apps.education.fr/nuit-du-code/DOCUMENTATION/-/raw/main/docs/assets/documents/regles-et-conseils-scratch.pdf" download>Scratch</a>
    * Règles et conseils <a href="https://forge.apps.education.fr/nuit-du-code/DOCUMENTATION/-/raw/main/docs/assets/documents/regles-et-conseils-python.pdf" download>Python / Pyxel</a>
    * Documentation <a href="https://forge.apps.education.fr/nuit-du-code/DOCUMENTATION/-/raw/main/docs/assets/documents/documentation-pyxel.pdf" download>Pyxel</a>
    * Carnet de bord <a href="https://forge.apps.education.fr/nuit-du-code/DOCUMENTATION/-/raw/main/docs/assets/documents/carnet-de-bord-scratch.pdf" download>Scratch</a>
    * Carnet de bord <a href="https://forge.apps.education.fr/nuit-du-code/DOCUMENTATION/-/raw/main/docs/assets/documents/carnet-de-bord-python.pdf" download>Python / Pyxel</a>

## 😺 SCRATCH

Durant la NDC, vous disposerez de 6 heures pour créer un jeu avec Scratch. Pour cela, vous devrez utiliser l'un des univers de jeu mis à votre disposition (fichiers <kbd>.sb3</kbd>). Ces univers de jeu ne contiennent aucun script, mais possèdent de nombreux lutins, des scènes et des sons.


### LES RÈGLES

!!! danger "IMPORTANT"
    * Les univers de jeu et les liens fournis sont **_confidentiels_**. Pendant la période de la NDC (mai - juin):
        * Les univers de jeu et les liens fournis ne doivent être partagés avec personne d’autre.
        * Les univers de jeu ne doivent pas être utilisés pour créer d’autres jeux.
    * **NE PAS PARTAGER VOTRE JEU SUR LE SITE DE SCRATCH**. Si vous avez cliqué sur « Share » par mégarde, allez dans « My Stuff » et cliquez sur « Unshare ». Un contrôle automatique est fait régulièrement. Les jeux partagés sur le site de Scratch ne seront pas évalués.
    * Le respect des consignes fait partie de l’évaluation du jeu.


* <span style="font-weight:bold"><span style="background-color:#ffde59">Vous devez</span> choisir un univers de jeu parmi ceux proposés (fichiers <kbd>.sb3</kbd>)</span>. Prenez le temps de bien étudier les différents univers de jeu (lutins, costumes, décors…) avant d'en choisir un. Cet univers devient le point de départ de votre jeu. Vous ne pouvez créer votre jeu qu'à partir des éléments que vous trouvez dans l'univers de jeu que vous avez choisi sans y ajouter aucun élément extérieur.
* <span style="font-weight:bold"><span style="background-color:#ffde59">Vous devez</span> écrire une courte présentation et mode d'emploi de votre jeu (<u>en français</u>)</span>.
* <span style="font-weight:bold;background-color:#ffaa95">Il n'est pas autorisé</span> d'importer des lutins, scènes ou sons (de la bibliothèque Scratch, depuis Internet ou votre ordinateur) dans votre projet. Seuls ceux fournis dans l'univers de jeu doivent être utilisés.
* <span style="font-weight:bold;background-color:#ffaa95">Il n'est pas autorisé</span> de regarder ou de copier/coller des scripts d'autres projets trouvés sur la plateforme Scratch ou sur votre ordinateur.</span>
* <span style="font-weight:bold;background-color:#ffaa95">Il n'est pas autorisé</span> d'aller chercher des informations ou de l'aide sur internet ou sur votre ordinateur.
* <span style="font-weight:bold;background-color:#ffaa95">Il n'est pas autorisé</span> d'utiliser des notes personnelles ou de la documentation papier.
* <span style="font-weight:bold;background-color:#bbe33d">Il est autorisé</span>, et même conseillé, de demander de l'aide aux enseignants qui encadrent la NDC. Ils ne vous donneront pas un code complet, mais certainement de bons conseils pour avancer.
* <span style="font-weight:bold;background-color:#bbe33d">Il est autorisé</span>, et même conseillé, de demander de l'aide à vos camarades des autres équipes participantes. La NDC est un événement festif et l'entraide est fortement recommandée.
* Vous avez le droit de dupliquer des lutins si vous en avez besoin.
* Les éléments du jeu vous suggèrent peut-être de créer un jeu que vous connaissez déjà, mais vous êtes tout à fait libre de proposer autre chose.
* Vous avez le droit de faire des « retouches » graphiques mineures sur les lutins ou les scènes: symétrie, rotation, changer une couleur, déformer, ajout de texte… mais attention à ne pas y passer trop de temps. Il est quand même conseillé d'éviter: il y a suffisamment de lutins (et certains ont beaucoup de costumes) pour trouver ce dont vous avez besoin.
* Vous avez le droit de créer des lutins qui serviraient de « capteur » lors des déplacements (juste un rectangle ou un cercle) ou des lutins « texte ».
* Vous n'êtes pas obligé d'utiliser tous les lutins, scènes ou sons de l'univers de jeu.


### QUELQUES CONSEILS
* Avant de vous lancer dans le code, prenez le temps d'imaginer votre jeu. Passez en revue tous les lutins et tous leurs costumes (qui sont parfois nombreux). Prévoyez de réaliser rapidement une version simple, mais jouable de votre jeu. Puis, si vous en avez le temps, rajoutez au fur et à mesure des éléments de complexité: niveau de difficulté, scores, son, etc.
* Vous travaillez à deux ou trois: organisez-vous pour être les plus efficaces possible.
* **Évitez les jeux multijoueurs**.  
* **Pensez à sauvegarder** ! Et surtout, effectuez régulièrement des **copies incrémentées** (version 1, 2, 3…) de votre jeu à chaque amélioration majeure (qui marche). <u>Scratch ne permet pas d'annuler des modifications</u>.
* N'oubliez pas de faire des pauses, d'aller voir ce que font les autres, de boire et de manger !
* <span style="font-weight:bold;color:#5eb91e">Et surtout, rappelez-vous: c'est un jeu ! Amusez-vous !</span>

### IMPORTANT: À LA FIN DES 6h

**Déposez votre jeu** sur le site de la NDC en utilisant le lien fourni par votre enseignant. Ce lien vous amènera sur la page de dépôt.

Sur cette page, vous devrez indiquer le nom de votre équipe, sélectionner votre catégorie, déposer votre fichier <kbd>.sb3</kbd> et compléter le champ « Présentation / Mode d’emploi du jeu ».

Pour récupérer le fichier <kbd>.sb3</kbd> de votre jeu, cliquez sur « File » et puis cliquez sur « Save to your computer ». En cas de problème, demandez l’aide de vote enseignant.
Si vous n'avez pas la possibilité de déposer vous-même le fichier sur le site, donnez-le à votre enseignant.

###  CRITÈRES D'ÉVALUATION

Chaque critère est noté sur 5:

* Jouabilité  `~ un jeu qui ne marche pas ne sera pas évalué ~`
* Richesse / Complexité
* Originalité / Créativité
* Respect des consignes / Présentation / Mode d'emploi


## 🐍 PYTHON / PYXEL

Durant la NDC, vous disposerez de 6 heures pour créer un jeu avec Python / Pyxel. Pour cela, vous pouvez utiliser les ressources mises à votre disposition (fichiers <kbd>.pyxres</kbd>) ou respecter un thème en partant d'un fichier <kbd>.pyxres</kbd> vide (ou en n'utilisant que des formes géométriques).

### LES RÈGLES

!!! danger "IMPORTANT"
    * Les univers de jeu et les liens fournis sont **_confidentiels_**. Pendant la période de la NDC (mai - juin):
        * Les univers de jeu et les liens fournis ne doivent être partagés avec personne d’autre.
        * Les univers de jeu ne doivent pas être utilisés pour créer d’autres jeux.
    * Le respect des consignes fait partie de l’évaluation du jeu.  

* <span style="font-weight:bold"><span style="background-color:#ffde59">Vous devez</span> choisir un univers de jeu parmi ceux proposés (fichiers <kbd>.pyxres</kbd>) ou respecter un thème</span>. Prenez le temps de bien étudier les différents univers de jeu avant d'en choisir un. Si vous préférez un thème, vous pouvez utiliser le fichier <kbd>res.pyxres</kbd> vide ou n'utiliser que des formes géométriques.
* <span style="font-weight:bold"><span style="background-color:#ffde59">Vous devez</span> écrire une courte présentation et mode d'emploi de votre jeu (<u>en français</u>)</span>.
* <span style="font-weight:bold"><span style="background-color:#ffde59">Vous devez</span> fixer la taille de l’écran à 128x128 ou 256x256 pixels</span> (<kbd>pyxel.init(128, 128, title="Nuit du Code")</kbd> ou <kbd>pyxel.init(256, 256, title="Nuit du Code")</kbd>).
* <span style="font-weight:bold"><span style="background-color:#ffde59">Vous devez</span> mettre le code de votre jeu dans <u>un seul fichier</u> <kbd>.py</kbd></span>. Si vous utilisez des images, elles seront dans un deuxième fichier au format <kbd>.pyxres</kbd>.
* <span style="font-weight:bold;background-color:#ffaa95">Il n'est pas autorisé</span> de regarder ou de copier/coller du code trouvé sur internet ou sur votre ordinateur.
* <span style="font-weight:bold;background-color:#ffaa95">Il n'est pas autorisé</span> d'importer des fichiers contenant du code (fichiers <kbd>.py</kbd>) ou des images (fichiers <kbd>.pyxres</kbd>).
* <span style="font-weight:bold;background-color:#ffaa95">Il n'est pas autorisé</span> de modifier les images des banques d'images des fichiers <kbd>.pyxres</kbd> fournis (par contre, vous pouvez utiliser librement les éditeurs de "tilemaps", de sons et de musiques).
* <span style="font-weight:bold;background-color:#ffaa95">Il n'est pas autorisé</span> d'aller chercher des informations ou de l'aide sur internet ou sur votre ordinateur.
* <span style="font-weight:bold;background-color:#ffaa95">Il n'est pas autorisé</span> d'utiliser des notes personnelles ou de la documentation papier autre que celle fournie.
* <span style="font-weight:bold;background-color:#bbe33d">Il est autorisé</span>, et même conseillé, de demander de l'aide aux enseignants qui encadrent la NDC. Ils ne vous donneront pas un code complet, mais certainement de bons conseils pour avancer.
* <span style="font-weight:bold;background-color:#bbe33d">Il est autorisé</span>, et même conseillé, de demander de l'aide à vos camarades des autres équipes participantes. La NDC est un événement festif et l'entraide est fortement recommandée.
* Vous n'êtes pas obligé d'utiliser tous les éléments graphiques du fichier <kbd>.pyxres</kbd>.

### QUELQUES CONSEILS

* Avant de vous lancer dans le code, prenez le temps d'imaginer votre jeu. Passez en revue les ressources. Prévoyez de réaliser rapidement une version simple, mais jouable de votre jeu. Puis, si vous en avez le temps, rajoutez au fur et à mesure des éléments de complexité: niveau de difficulté, scores, son, etc.
* Vous travaillez à deux ou trois: organisez-vous pour être les plus efficaces possible.
* **Évitez les jeux multijoueurs**.
* **Pensez à sauvegarder** ! Et surtout, effectuez régulièrement **des copies incrémentées** (version 1, 2, 3…) de votre jeu à chaque amélioration majeure (qui marche) en téléchargeant le ou les fichiers du jeu.
* N'oubliez pas de faire des pauses, d'aller voir ce que font les autres, de boire et de manger !
* <span style="font-weight:bold;color:#5eb91e">Et surtout, rappelez-vous: c'est un jeu ! Amusez-vous !</span>

### IMPORTANT: À LA FIN DES 6h

**Déposez votre jeu** sur le site de la NDC en utilisant le lien fourni par votre enseignant. Ce lien vous amènera sur la page de dépôt.

Sur cette page, vous devrez indiquer le nom de votre équipe, sélectionner votre catégorie, déposer votre fichier <kbd>.py</kbd> et, s'il existe, le fichier <kbd>.pyxres</kbd> et compléter le champ « Présentation / Mode d'emploi du jeu ».

Si vous n'avez pas la possibilité de déposer vous-même ces fichiers sur le site, donnez-les à votre enseignant.

###  CRITÈRES D'ÉVALUATION

Chaque critère est noté sur 5:

* Jouabilité  `~ un jeu qui ne marche pas ne sera pas évalué ~`
* Richesse / Complexité
* Originalité / Créativité
* Respect des consignes / Présentation / Mode d'emploi
