---
hide:
  - footer
---

# MATÉRIEL & LOGICIELS

## MATÉRIEL
Les élèves doivent avoir, à minima:

* un poste fixe ou un portable
* des écouteurs

## ENVIRONNEMENTS EN LIGNE
Si une bonne connexion internet est disponible, aucun logiciel n'est à installer. Vous pouvez utiliser les environnements en ligne suivants.

### 😺 Scratch
> Site de Scratch: https://scratch.mit.edu/

Les élèves ne sont pas obligés de créer un compte. 
Cependant, s'ils souhaitent se partager le travail en utilisant le système de [sac à dos](https://en.scratch-wiki.info/wiki/Backpack) (voir aussi cette [vidéo](https://www.youtube.com/watch?v=AuolBPY8qdc)) de Scratch, ils doivent avoir un compte.

Conseils:

* créer les comptes bien avant la Nuit du Code afin d'avoir le temps de les valider (le courriel de validation met parfois beaucoup de temps à arriver).
* avec ou sans compte, faire des sauvegardes locales régulières des jeux.

### 🐍 Python / Pyxel
> Site Pyxel Studio: http://pyxelstudio.net/

Les élèves ne sont pas obligés de créer un compte. 

Conseil: faire des sauvegardes locales régulières des jeux.


## LOGICIELS

Les environnements en ligne sont pratiques mais il est cependant recommandé d'installer les logiciels suivants afin d'éviter tout problème lié à une coupure de connexion (wifi, internet, serveurs...).

### 😺 Scratch

Voir: [scratch.mit.edu/download](https://scratch.mit.edu/download)


### 🐍 Python / Pyxel

* Pour Windows

    * Télécharger [Edupyter](https://www.edupyter.net/)
    * Installer Edupyter<br />
    Edupyter est portable et ne nécessite pas de droits administrateur. Edupyter peut être installé dans un dossier sur le disque dur ou sur une clé USB / disque amovible. L'installation ne fait que copier des fichiers et des dossiers dans le dossier d'installation choisi. Le système n'est pas modifié. Pour supprimer Edupyter il suffit de supprimer le dossier dans lequel il a été installé.
    * Lancer Edupyter et cliquer sur l'icône qui apparaît à côté de l'horloge (voir [aide](https://raw.githubusercontent.com/edupyter/documentation/main/edupyter-help.png)).
    * Ouvrir Thonny
    * Saisir le code et enregistrer le fichier Python (en <kbd>.py</kbd>).
    * Exécuter le code en cliquant sur la touche F5 ou le bouton vert. Cette action ouvrira automatiquement la fenêtre du jeu.
    * Pour créer un fichier <kbd>.pyxres</kbd>, ouvrir la console, naviguer jusqu'au répertoire qui contient le fichier <kbd>.py</kbd> et saisir <kbd>pyxel edit nom-du-fichier.pyxres</kbd>.

* Pour Windows / Mac / Linux: voir [Comment installer Pyxel](https://github.com/kitao/pyxel/blob/main/docs/README.fr.md#comment-installer).


## VERSIONS

!!! danger "😺 SCRATCH"

    Scratch 3


!!! danger "🐍 PYTHON/PYXEL"

    Python 3 et Pyxel 1.8 minimum


## FICHIERS À RENDRE

Pour pouvoir utiliser l'environnement d'évaluation des jeux du site de la NDC et/ou pour participer à la sélection internationale, les élèves devront exporter leur jeu et le déposer sur le site grâce au lien fourni par l'enseignant responsable de l'organisation.  

!!! info "😺 SCRATCH"

    Exporter et déposer le fichier <kbd>.sb3</kbd>.


!!! info "🐍 PYTHON/PYXEL"

    Exporter et déposer le fichier <kbd>.py</kbd> et, éventuellement, le fichier <kbd>.pyxres</kbd>.
