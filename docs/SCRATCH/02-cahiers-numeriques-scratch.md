# TUTORIELS - VIDÉOS + ENVIRONNEMENT SCRATCH

**Entraînements en autonomie**

> 18 tutoriels au format "cahier numérique" : vidéo + environnement Scratch
> 
> Les élèves ont ainsi sur une même page la vidéo sur la partie gauche et l'environnement Scratch sur la partie droite. Il leur est alors possible de mettre directement en application ce qu'ils voient sur la vidéo. Ceci permet un entraînement en autonomie. Il suffit de fournir les liens aux élèves.
> 
> Si vous souhaitez créer vous même des cahiers numériques: <a href="https://www.cahiernum.net" target="_blank">Cahiers Numériques</a>
>
> Vidéos créées par <a href="https://www.monclasseurdemaths.fr" target="_blank">Jean-Yve Labouche</a>

* 1 - Déplacer un lutin avec les touches du clavier (méthode 1) : [www.cahiernum.net/4WMPL6](https://www.cahiernum.net/4WMPL6)
* 2 - Déplacer un lutin avec les touches du clavier (méthode 2) : [www.cahiernum.net/ED23TF](https://www.cahiernum.net/ED23TF)
* 3 - Bien commencer son programme: l'initialisation : [www.cahiernum.net/GF3YPT](https://www.cahiernum.net/GF3YPT)
* 4 - Déplacer un lutin automatiquement vers un autre lutin : [www.cahiernum.net/WVHS7Z](https://www.cahiernum.net/WVHS7Z)
* 5 - Obtenir des déplacements plus fluides : [www.cahiernum.net/69ZWPJ](https://www.cahiernum.net/69ZWPJ)
* 6 - Ajouter des interactions entre les lutins : [www.cahiernum.net/32QZGE](https://www.cahiernum.net/32QZGE)
* 7 - Ajouter des interactions entre un luton et l'arrière plan : [www.cahiernum.net/EG8XCJ](https://www.cahiernum.net/EG8XCJ)
* 8 - Faire communiquer des lutins : [www.cahiernum.net/9RZQ83](https://www.cahiernum.net/9RZQ83)
* 9 - Utiliser le chronomètre : [www.cahiernum.net/W7YXD8](https://www.cahiernum.net/W7YXD8)
* 10 - Créer un tir (utilisation des clones) : [www.cahiernum.net/ENZUSD](https://www.cahiernum.net/ENZUSD)
* 11 - Faire sauter un lutin : [www.cahiernum.net/3Z7A9S](https://www.cahiernum.net/3Z7A9S)
* 12 - Faire sauter un lutin: version améliorée : [www.cahiernum.net/CW3S6T](https://www.cahiernum.net/CW3S6T)
* 13 - Déplacements plus réalistes (pas d'arrêts brusques) : [www.cahiernum.net/ZWKQPX](https://www.cahiernum.net/ZWKQPX)
* 14 - Créer une trajectoire parabolique (lancer de ballon, tir...) : [www.cahiernum.net/XU6ZGF](https://www.cahiernum.net/XU6ZGF)
* 15 - Faire défiler l'arrière plan (scrolling) : [www.cahiernum.net/M6JG7H](https://www.cahiernum.net/M6JG7H)
* 16 - Faire défiler l'arrière plan pour donner un effet de profondeur : [www.cahiernum.net/ANVH93](https://www.cahiernum.net/ANVH93)
* 17 - Réaliser un scrolling fini sur plusieurs arrière-plans : [www.cahiernum.net/EZD9PA](https://www.cahiernum.net/EZD9PA)
* 18 - Réaliser un scrolling infini sur plusieurs arrière-plans : [www.cahiernum.net/J7KFMX](https://www.cahiernum.net/J7KFMX)


<table cellpadding="5">
<tr>
<td>
 
<a href="https://www.cahiernum.net/4WMPL6"><img src="https://forge.apps.education.fr/nuit-du-code/DOCUMENTATION/-/raw/main/docs/assets/images/tutoriels/tutoriel_scratch/tutoriel_scratch_episode-01.png" /></a>
 
</td>
<td>

<a href="https://www.cahiernum.net/ED23TF"><img src="https://forge.apps.education.fr/nuit-du-code/DOCUMENTATION/-/raw/main/docs/assets/images/tutoriels/tutoriel_scratch/tutoriel_scratch_episode-02.png" /></a>

</td>
<td>

<a href="https://www.cahiernum.net/GF3YPT"><img src="https://forge.apps.education.fr/nuit-du-code/DOCUMENTATION/-/raw/main/docs/assets/images/tutoriels/tutoriel_scratch/tutoriel_scratch_episode-03.png" /></a>

</td>
</tr>

<tr>
<td>

<a href="https://www.cahiernum.net/WVHS7Z"><img src="https://forge.apps.education.fr/nuit-du-code/DOCUMENTATION/-/raw/main/docs/assets/images/tutoriels/tutoriel_scratch/tutoriel_scratch_episode-04.png" /></a>

</td>
<td>

<a href="https://www.cahiernum.net/69ZWPJ"><img src="https://forge.apps.education.fr/nuit-du-code/DOCUMENTATION/-/raw/main/docs/assets/images/tutoriels/tutoriel_scratch/tutoriel_scratch_episode-05.png" /></a>

</td>
<td>

<a href="https://www.cahiernum.net/32QZGE"><img src="https://forge.apps.education.fr/nuit-du-code/DOCUMENTATION/-/raw/main/docs/assets/images/tutoriels/tutoriel_scratch/tutoriel_scratch_episode-06.png" /></a>

</td>
</tr>

<tr>
<td>

<a href="https://www.cahiernum.net/EG8XCJ"><img src="https://forge.apps.education.fr/nuit-du-code/DOCUMENTATION/-/raw/main/docs/assets/images/tutoriels/tutoriel_scratch/tutoriel_scratch_episode-07.png" /></a>

</td>
<td>

<a href="https://www.cahiernum.net/9RZQ83"><img src="https://forge.apps.education.fr/nuit-du-code/DOCUMENTATION/-/raw/main/docs/assets/images/tutoriels/tutoriel_scratch/tutoriel_scratch_episode-08.png" /></a>
</td>
<td>

<a href="https://www.cahiernum.net/W7YXD8"><img src="https://forge.apps.education.fr/nuit-du-code/DOCUMENTATION/-/raw/main/docs/assets/images/tutoriels/tutoriel_scratch/tutoriel_scratch_episode-09.png" /></a>

</td>
</tr>
  
<tr>
<td>

<a href="https://www.cahiernum.net/ENZUSD"><img src="https://forge.apps.education.fr/nuit-du-code/DOCUMENTATION/-/raw/main/docs/assets/images/tutoriels/tutoriel_scratch/tutoriel_scratch_episode-10.png" /></a>

</td>
<td>

<a href="https://www.cahiernum.net/3Z7A9S"><img src="https://forge.apps.education.fr/nuit-du-code/DOCUMENTATION/-/raw/main/docs/assets/images/tutoriels/tutoriel_scratch/tutoriel_scratch_episode-11.png" /></a>

</td>
<td>

<a href="https://www.cahiernum.net/CW3S6T"><img src="https://forge.apps.education.fr/nuit-du-code/DOCUMENTATION/-/raw/main/docs/assets/images/tutoriels/tutoriel_scratch/tutoriel_scratch_episode-12.png" /></a>

</td>
</tr>
  
<tr>
<td>

<a href="https://www.cahiernum.net/ZWKQPX"><img src="https://forge.apps.education.fr/nuit-du-code/DOCUMENTATION/-/raw/main/docs/assets/images/tutoriels/tutoriel_scratch/tutoriel_scratch_episode-13.png" /></a>

</td>
<td>

<a href="https://www.cahiernum.net/XU6ZGF"><img src="https://forge.apps.education.fr/nuit-du-code/DOCUMENTATION/-/raw/main/docs/assets/images/tutoriels/tutoriel_scratch/tutoriel_scratch_episode-14.png" /></a>

</td>
<td>

<a href="https://www.cahiernum.net/M6JG7H"><img src="https://forge.apps.education.fr/nuit-du-code/DOCUMENTATION/-/raw/main/docs/assets/images/tutoriels/tutoriel_scratch/tutoriel_scratch_episode-15.png" /></a>

</td>
</tr>  
  
<tr>
<td>

<a href="https://www.cahiernum.net/ANVH93"><img src="https://forge.apps.education.fr/nuit-du-code/DOCUMENTATION/-/raw/main/docs/assets/images/tutoriels/tutoriel_scratch/tutoriel_scratch_episode-16.png" /></a>

</td>
<td>

<a href="https://www.cahiernum.net/EZD9PA"><img src="https://forge.apps.education.fr/nuit-du-code/DOCUMENTATION/-/raw/main/docs/assets/images/tutoriels/tutoriel_scratch/tutoriel_scratch_episode-17.png" /></a>

</td>
<td>

<a href="https://www.cahiernum.net/J7KFMX"><img src="https://forge.apps.education.fr/nuit-du-code/DOCUMENTATION/-/raw/main/docs/assets/images/tutoriels/tutoriel_scratch/tutoriel_scratch_episode-18.png" /></a>

</td>
</tr>   
  
</table>
