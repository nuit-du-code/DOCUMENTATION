# TUTORIELS - VIDÉOS

> 18 tutoriels au format vidéo
> 
> Vidéos créées par <a href="https://www.monclasseurdemaths.fr" target="_blank">Jean-Yve Labouche</a>

<table cellpadding="5">
<tr>

<td>
 
<a href="https://www.youtube.com/watch?v=a6WRTBb8CkM"><img src="https://forge.apps.education.fr/nuit-du-code/DOCUMENTATION/-/raw/main/docs/assets/images/tutoriels/tutoriel_scratch/tutoriel_scratch_episode-01.png" /></a>
 
</td>
<td>

<a href="https://www.youtube.com/watch?v=3PbXyzQQOYk"><img src="https://forge.apps.education.fr/nuit-du-code/DOCUMENTATION/-/raw/main/docs/assets/images/tutoriels/tutoriel_scratch/tutoriel_scratch_episode-02.png" /></a>

</td>
<td>

<a href="https://www.youtube.com/watch?v=8oSKtPQNDr8"><img src="https://forge.apps.education.fr/nuit-du-code/DOCUMENTATION/-/raw/main/docs/assets/images/tutoriels/tutoriel_scratch/tutoriel_scratch_episode-03.png" /></a>

</td>
</tr>

<tr>
<td>

<a href="https://www.youtube.com/watch?v=xRiZD5wsh0k"><img src="https://forge.apps.education.fr/nuit-du-code/DOCUMENTATION/-/raw/main/docs/assets/images/tutoriels/tutoriel_scratch/tutoriel_scratch_episode-04.png" /></a>

</td>
<td>

<a href="https://www.youtube.com/watch?v=O1tDjDGwBtE"><img src="https://forge.apps.education.fr/nuit-du-code/DOCUMENTATION/-/raw/main/docs/assets/images/tutoriels/tutoriel_scratch/tutoriel_scratch_episode-05.png" /></a>

</td>
<td>

<a href="https://www.youtube.com/watch?v=nINGvQVP_Og"><img src="https://forge.apps.education.fr/nuit-du-code/DOCUMENTATION/-/raw/main/docs/assets/images/tutoriels/tutoriel_scratch/tutoriel_scratch_episode-06.png" /></a>

</td>
</tr>

<tr>
<td>

<a href="https://www.youtube.com/watch?v=P22s9A9U9lM"><img src="https://forge.apps.education.fr/nuit-du-code/DOCUMENTATION/-/raw/main/docs/assets/images/tutoriels/tutoriel_scratch/tutoriel_scratch_episode-07.png" /></a>

</td>
<td>

<a href="https://www.youtube.com/watch?v=jtVMnEZDDes"><img src="https://forge.apps.education.fr/nuit-du-code/DOCUMENTATION/-/raw/main/docs/assets/images/tutoriels/tutoriel_scratch/tutoriel_scratch_episode-08.png" /></a>

</td>
<td>

<a href="https://www.youtube.com/watch?v=v37gPSxlPW4"><img src="https://forge.apps.education.fr/nuit-du-code/DOCUMENTATION/-/raw/main/docs/assets/images/tutoriels/tutoriel_scratch/tutoriel_scratch_episode-09.png" /></a>

</td>
</tr>
  
<tr>
<td>

<a href="https://www.youtube.com/watch?v=P7ORrWLlGsA"><img src="https://forge.apps.education.fr/nuit-du-code/DOCUMENTATION/-/raw/main/docs/assets/images/tutoriels/tutoriel_scratch/tutoriel_scratch_episode-10.png" /></a>

</td>
<td>

<a href="https://www.youtube.com/watch?v=n1NCXKKrtq0"><img src="https://forge.apps.education.fr/nuit-du-code/DOCUMENTATION/-/raw/main/docs/assets/images/tutoriels/tutoriel_scratch/tutoriel_scratch_episode-11.png" /></a>

</td>
<td>

<a href="https://www.youtube.com/watch?v=DZYkdUrQegU"><img src="https://forge.apps.education.fr/nuit-du-code/DOCUMENTATION/-/raw/main/docs/assets/images/tutoriels/tutoriel_scratch/tutoriel_scratch_episode-12.png" /></a>

</td>
</tr>
  
<tr>
<td>

<a href="https://www.youtube.com/watch?v=5wrleesBQCU"><img src="https://forge.apps.education.fr/nuit-du-code/DOCUMENTATION/-/raw/main/docs/assets/images/tutoriels/tutoriel_scratch/tutoriel_scratch_episode-13.png" /></a>

</td>
<td>

<a href="https://www.youtube.com/watch?v=MK4F1E3_nEY"><img src="https://forge.apps.education.fr/nuit-du-code/DOCUMENTATION/-/raw/main/docs/assets/images/tutoriels/tutoriel_scratch/tutoriel_scratch_episode-14.png" /></a>

</td>
<td>

<a href="https://www.youtube.com/watch?v=fQZVY39WTg4"><img src="https://forge.apps.education.fr/nuit-du-code/DOCUMENTATION/-/raw/main/docs/assets/images/tutoriels/tutoriel_scratch/tutoriel_scratch_episode-15.png" /></a>

</td>
</tr>  
  
<tr>
<td>

<a href="https://www.youtube.com/watch?v=8VfQzEOcRd0"><img src="https://forge.apps.education.fr/nuit-du-code/DOCUMENTATION/-/raw/main/docs/assets/images/tutoriels/tutoriel_scratch/tutoriel_scratch_episode-16.png" /></a>

</td>
<td>

<a href="https://www.youtube.com/watch?v=-S4utl_quqQ"><img src="https://forge.apps.education.fr/nuit-du-code/DOCUMENTATION/-/raw/main/docs/assets/images/tutoriels/tutoriel_scratch/tutoriel_scratch_episode-17.png" /></a>

</td>
<td>

<a href="https://www.youtube.com/watch?v=X669S6tAyis"><img src="https://forge.apps.education.fr/nuit-du-code/DOCUMENTATION/-/raw/main/docs/assets/images/tutoriels/tutoriel_scratch/tutoriel_scratch_episode-18.png" /></a>

</td>
</tr>   
  
</table>
