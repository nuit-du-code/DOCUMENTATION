# INTRODUCTION

Pour préparer la Nuit du Code avec les élèves, voici plusieurs ressources qui permettent de mettre en place des séances de découverte / approfondissement de Scratch **encadrées** ou en **autonomie**.

* 18 [tutoriels au format "cahier numérique"](/DOCUMENTATION/SCRATCH/02-cahiers-numeriques-scratch)
* 18 [tutoriels vidéo](/DOCUMENTATION/SCRATCH/03-videos)
* 4 [exercices d'entraînement ou de sélection](/DOCUMENTATION/SCRATCH/04-entrainements-selections)
* Univers de jeu Scratch [2023](/DOCUMENTATION/SCRATCH/05-univers-scratch-2023)
* Univers de jeu Scratch [2024](/DOCUMENTATION/SCRATCH/06-univers-scratch-2024)
