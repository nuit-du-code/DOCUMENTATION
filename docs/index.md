# 🗃️ SOMMAIRE

<br />

:fontawesome-solid-arrow-left: retour sur le [site de la **Nuit du Code**](https://www.nuitducode.net/)

<br />

* 📑 [PRÉSENTATION](01-presentation/)

* 🧩 [ORGANISATION](02-organisation/)

* 🎁 [COMMUNICATION & "GOODIES"](03-communication-et-goodies/)

* 💡 [RÈGLES ET CONSEILS](04-regles-conseils/)

* 💻 [MATÉRIEL ET LOGICIELS](05-materiel-logiciels/)

* 🐍 [TUTORIELS PYTHON / PYXEL](PYTHON/01-presentation/)

* 🐱 [TUTORIELS SCRATCH](SCRATCH/01-introduction/)