# UNIVERS 2024

<div class="grid cards" markdown>

-   ![1.pyxres](https://www.nuitducode.net/univers_python/2024/1.png){ align=center }

    [1.pyxres](https://www.nuitducode.net/univers_python/2024/1.pyxres)

-   ![2.pyxres](https://www.nuitducode.net/univers_python/2024/2.png){ align=center }

    [2.pyxres](https://www.nuitducode.net/univers_python/2024/2.pyxres)

-   ![3.pyxres](https://www.nuitducode.net/univers_python/2024/3.png){ align=center }

    [3.pyxres](https://www.nuitducode.net/univers_python/2024/3.pyxres)

-   ![4.pyxres](https://www.nuitducode.net/univers_python/2024/4.png){ align=center }

    [4.pyxres](https://www.nuitducode.net/univers_python/2024/4.pyxres)

</div>