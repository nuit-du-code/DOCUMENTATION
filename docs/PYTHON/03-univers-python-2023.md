# UNIVERS 2023

<div class="grid cards" markdown>

-   ![1.pyxres](https://www.nuitducode.net/univers_python/2023/1.png){ align=center }

    [1.pyxres](https://www.nuitducode.net/univers_python/2023/1.pyxres)

-   ![2.pyxres](https://www.nuitducode.net/univers_python/2023/2.png){ align=center }

    [2.pyxres](https://www.nuitducode.net/univers_python/2023/2.pyxres)

-   ![3.pyxres](https://www.nuitducode.net/univers_python/2023/3.png){ align=center }

    [3.pyxres](https://www.nuitducode.net/univers_python/2023/3.pyxres)

-   ![4.pyxres](https://www.nuitducode.net/univers_python/2023/4.png){ align=center }

    [4.pyxres](https://www.nuitducode.net/univers_python/2023/4.pyxres)

-   ![5.pyxres](https://www.nuitducode.net/univers_python/2023/5.png){ align=center }

    [5.pyxres](https://www.nuitducode.net/univers_python/2023/5.pyxres)

</div>
